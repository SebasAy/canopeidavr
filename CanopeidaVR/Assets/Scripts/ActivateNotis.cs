using UnityEngine;

public class ActivateNotis : MonoBehaviour
{
    public GameObject objetoAActivar; 
    [SerializeField]
    private float distanciaActivacion = 5f;

    private GameObject jugador;
    private bool jugadorCerca = false;

    void Start()
    {
        jugador = GameObject.FindGameObjectWithTag("Player");
    }

    void Update()
    {
        if (jugador != null)
        {
            float distanciaAlJugador = Vector3.Distance(transform.position, jugador.transform.position);

            if (distanciaAlJugador <= distanciaActivacion)
            {
                if (!jugadorCerca)
                {
                    objetoAActivar.SetActive(true);
                    jugadorCerca = true;
                }
            }
            else
            {
                if (jugadorCerca)
                {
                    objetoAActivar.SetActive(false); 
                    jugadorCerca = false;
                }
            }
        }
    }
}
