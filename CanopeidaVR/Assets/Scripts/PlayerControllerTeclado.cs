using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerTeclado : MonoBehaviour
{
    private new Rigidbody rigidbody;

    public float movementSpeed;
    public float rotationSpeed;

    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
    }

    void Update()
    {
        // Obtener el movimiento horizontal del usuario
        float hor = Input.GetAxisRaw("Horizontal");

        // Rotar el jugador en lugar de moverlo lateralmente
        transform.Rotate(Vector3.up, hor * rotationSpeed * Time.deltaTime);

        // Mantener la velocidad vertical
        Vector3 velocity = new Vector3(0, rigidbody.velocity.y, 0);

        // Aplicar movimiento vertical
        float ver = Input.GetAxisRaw("Vertical");
        if (ver != 0)
        {
            velocity += transform.forward * ver * movementSpeed;
        }

        // Aplicar la velocidad al Rigidbody
        rigidbody.velocity = velocity;
    }
}
