using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LogicaCharcos : MonoBehaviour
{

    public int numCharcos;
    public TextMeshProUGUI textoMision;
    public GameObject botonMision;




    // Start is called before the first frame update
    void Start()
    {
        numCharcos = GameObject.FindGameObjectsWithTag("Charco").Length;
        textoMision.text = "Recoge los " + numCharcos + " charcos que hay en la cubierta.";
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Charco")
        {
            Destroy(col.transform.parent.gameObject);
            numCharcos--;
            textoMision.text = "Recoge los " + numCharcos + " charcos que hay en la cubierta.";
            if (numCharcos <= 0)
            {
                textoMision.text = "Bien hecho";
                botonMision.SetActive(true);
            }
        }
    }
}
